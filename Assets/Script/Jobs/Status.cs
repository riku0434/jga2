using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status : MonoBehaviour
{
    protected int hp = 8;
    protected int mp = 8;
    protected int atk = 8;
    protected int matk = 8;
    protected int def = 8;
    protected int spd = 8; 
    protected int lv = 1; 
    protected int exp = 0;

	private int LvHp = 0;
	private int LvMp = 0;
	private int LvAtk = 0;
	private int LvMatk = 0;
	private int LvDef = 0;
	private int LvSpd = 0;

	protected void LevelUp()
	{
		hp += LvHp + Random.Range(0, 2 + lv / 8);
		mp += LvMp + Random.Range(0, 2 + lv / 8);
		atk += LvAtk + Random.Range(0, 2 + lv / 8);
		matk += LvMatk + Random.Range(0, 2 + lv / 8);
		def += LvDef + Random.Range(0, 2 + lv / 8);
		spd += LvSpd + Random.Range(0, 2 + lv / 8);
	}

	protected int LevelAddHp { set { LvHp = value; } }
	protected int LevelAddMp { set { LvMp = value; } }
	protected int LevelAddAtk { set { LvAtk = value; } }
	protected int LevelAddMatk { set { LvMatk = value; } }
	protected int LevelAddDef { set { LvDef = value; } }
	protected int LevelAddSpd { set { LvSpd = value; } }

}
